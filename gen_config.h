#ifndef GEN_CONFIG_H
#define GEN_CONFIG_H

#define VERSION "0.0.2"
#define LICENSE "GPL v2.0"
#define LICENSE_FILE "qrc:/"

#endif // GEN_CONFIG_H
